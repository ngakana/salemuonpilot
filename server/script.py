import sys, os
from collections import namedtuple
from unidecode import unidecode
import csv

csvStore = os.path.join(os.getcwd(), "data/fictitional-student-names.csv")
csvStore = "../data/fictitional-student-names.csv"

firstname = sys.argv[1]
lastname = sys.argv[2]
firstname = firstname.capitalize()
lastname = lastname.capitalize()
database = set()

def loadDatabase():
    # print(csvStore)
    with open(csvStore, newline='', encoding="utf-8") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")

            for row in reader:
                if row[0] == "Student":
                    continue
                else:
                   database.add(row[1])
    return database

def writeToDatabase(entry):
    # entry = f"{entry[0]},{entry[1]}"
    with open(csvStore, 'a', newline='', encoding="utf-8") as csfile:
        writer = csv.writer(csfile)
        writer.writerow(entry)
    

def validInput(name):
    if name.isalpha() :
        return True
    else:
        return False

def generateStudentNumber():
    """
    Generates UCT student number based on firstname and lastname
    """
    vowels = set(['A', 'E', 'I', 'O', 'U'])

    id_name = ""
    id_sur = ""

    # Format the firstname
    name_len = len(firstname)
    if name_len >= 3:
        id_name = firstname[:3].upper()
    else:
        id_name = firstname.upper()
        for i in range(3 - name_len):
            id_name += "X"

    # Format the lastname
    surname = unidecode(lastname).upper().replace(" ","")
    for char in surname:
        if char in vowels:
            surname = surname.replace(char,"")
        
    if len(surname) >= 3:
        id_sur = surname[:3]
    else:
        id_sur = surname
        for i in range(3 - len(surname)):
            id_sur += "X"

    # student number base string
    id_num_base = id_sur + id_name
    
    count = 1
    identifier = str(count).zfill(3)
    if(id_num_base+identifier) in database:
        while (id_num_base+identifier) in database:
            count += 1
            identifier = str(count).zfill(3)

    student_num = id_num_base+identifier
    student_entry = [f"{firstname} {lastname}",student_num]
    writeToDatabase(student_entry)
    
    print({ "names": student_entry[0] ,"id": student_entry[1] })

def main():
    loadDatabase()
    generateStudentNumber()

if __name__=="__main__":
    main()