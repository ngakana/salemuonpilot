# salemuonPilot #

## Todo ##
- [x] Curate dummy student data
- [ ] Backend
  - [x] Write the student number generator script
  - [ ] Integrate Python into Node.js
  - [ ] Setup API endpoint for the script
- [ ] Frontend
  - [ ] Write the HTML
  - [ ] Style the page
- [ ] Deploy salemuonPilot  